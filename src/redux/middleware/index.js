import thunk from 'redux-thunk';

const createMiddleware = () => [thunk];

export default createMiddleware;
