const INITIALIZE = (state) => ({
  ...state,
  isInitialized: true
});

export default INITIALIZE;
