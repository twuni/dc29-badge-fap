import { applyMiddleware, createStore } from 'redux';

import createMiddleware from './middleware';
import createReducer from './reducer';

const createRedux = (initialState = {}) => {
  const store = createStore(createReducer(initialState), applyMiddleware(...createMiddleware()));
  return store;
};

export default createRedux;
