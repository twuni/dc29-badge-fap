export const promisify = (object, functionName) => (...args) => new Promise((resolve, reject) => {
  object[functionName](...args, (error, result) => {
    if (error) {
      reject(error);
    } else {
      resolve(result);
    }
  });
});

export default promisify;
