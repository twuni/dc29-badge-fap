const IgnoredCharacterCode = [0, 12, 13].reduce((a, b) => {
  a[b] = true;
  return a;
}, {});

export const printable = (buffer) => buffer.filter((n) => !IgnoredCharacterCode[n]).toString('ascii');

export default printable;
