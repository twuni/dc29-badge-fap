import { Buffer } from 'buffer';
import SerialPort from 'serialport';

import { printable } from './printable';
import { promisify } from './promisify';

export const createSerialConnection = (options = {}) => {
  const port = new SerialPort(options.path || '/dev/ttyACM0', { autoOpen: false, baudRate: options.baudRate || 9600 });
  const close = promisify(port, 'close');
  const drain = promisify(port, 'drain');
  const open = promisify(port, 'open');

  return {
    connect: async (onDisconnect) => {
      await open();

      if (typeof onDisconnect === 'function') {
        port.on('close', onDisconnect);
      }

      const disconnect = async () => {
        await close();
      };

      const receive = (options = {}) => new Promise((resolve) => {
        const control = {};

        const state = {
          buffer: Buffer.allocUnsafe(options.bufferSize || 2048),
          size: 0
        };

        control.flush = () => {
          port.off('data', control.onData);
          const payload = state.buffer.slice(0, state.size);
          resolve(options.printable ? printable(payload) : payload);
        };

        control.onData = (data) => {
          state.size += data.copy(state.buffer, state.size, 0, data.length);

          if (state.bounce) {
            clearTimeout(state.bounce);
          }

          state.bounce = setTimeout(control.flush, options.timeout || 10);
        };

        port.on('data', control.onData);
      });

      const send = async (data, encoding) => {
        port.write(data, encoding);
        await drain();
      };

      const sendAndReceive = async (data, encoding) => {
        await send(data, encoding);
        return receive({ printable: Boolean(encoding) });
      };

      return {
        disconnect,
        receive,
        send,
        sendAndReceive
      };
    }
  };
};

export default createSerialConnection;
