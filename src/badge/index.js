import { Buffer } from 'buffer';

import { createSerialConnection } from '../serial-connection';

const BadgeType = [
  'Human',
  'Goon',
  'Creator',
  'Speaker',
  'Artist',
  'Vendor',
  'Press'
].reduce((a, b) => {
  a[b] = true;
  return a;
}, {});

export const createBadge = (options = {}) => {
  const { connect } = createSerialConnection({
    baudRate: options.baudRate || 9600,
    path: options.path || '/dev/ttyACM0'
  });

  const state = {};

  const parseStatus = (menu) => {
    const lines = menu.split('\n');

    for (const line of lines) {
      if (line.startsWith('Number of Badges Connected:')) {
        state.connectedBadgeCount = parseInt(line.replace(/^(.+): (.+)$/g, '$2'));
      } else if (line.startsWith('Badge Types Collected:')) {
        state.connectedBadgeTypes = line.split(': ')[1].split(',').filter(((type) => BadgeType[type]));
      } else if (line.startsWith('Times You\'ve Shared the Signal:')) {
        state.sharedSignalCount = parseInt(line.replace(/^(.+): (.+)$/g, '$2'));
      }
    }
  };

  return {
    connect: async (onDisconnect) => {
      const { disconnect, sendAndReceive } = await connect(onDisconnect);

      await sendAndReceive('\r', 'ascii');

      const generate = async () => {
        const response = await sendAndReceive('4', 'ascii');
        const lines = response.split('\n');
        return lines[2];
      };

      const receive = async (code) => {
        await sendAndReceive('5', 'ascii');
        const response = await sendAndReceive(`${code || ''}\r`, 'ascii');
        parseStatus(await sendAndReceive('\r', 'ascii'));
        const lines = response.split('\n');

        const errorMessage = lines.find((it) => it.startsWith(('ERROR') || it.startsWith('Invalid Input')));

        if (errorMessage) {
          throw new Error(errorMessage);
        }

        const replyCode = lines.find((line) => line !== code && (/^[0-9A-F]{32}$/g).test(line));

        if (replyCode) {
          return replyCode;
        }

        return undefined;
      };

      return {
        disconnect,
        generate,
        receive,
        status: async () => {
          if (state.connectedBadgeTypes) {
            return Promise.resolve(state);
          }

          try {
            await receive();
          } catch (ignore) {
            // To get back to the main menu to initialize the status, we intentionally receive an empty code.
          }

          return state;
        }
      };
    }
  };
};

export default createBadge;
