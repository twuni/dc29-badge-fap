import 'regenerator-runtime/runtime';

import createBadge from './badge';
import createRedux from './redux';
import { webcrypto } from 'crypto';
import initialize from './redux/actions/initialize';
import joinNetwork from './redux/actions/join-network';
import sendMessage from './redux/actions/send-message';
import setName from './redux/actions/set-name';

Object.assign(global, {
  crypto: webcrypto
});

const badge = createBadge({
  path: process.argv[2] || '/dev/ttyACM0'
});
const store = createRedux({ resources: [] });
const badgePairings = {};
const networkState = {};

const status = (({ status }) => `🔌 ${status.connectedBadgeCount} 📡 ${status.sharedSignalCount} 👥 ${status.connectedBadgeTypes.join(' | ')}`);
const greeting = ({ code }) => `✋ A request code for you: ${code}`;
const confirmReplyReceived = () => '✅ Reply code received!';
const yourReplyCode = ({ code }) => `✅ Your reply code: ${code}`;
const verboseBadgeStatus = (({ status }) => `💡 Status:
     Badges Connected : ${status.connectedBadgeCount}
     Signal Shared    : ${status.sharedSignalCount}
     Badge Types Seen : ${status.connectedBadgeTypes.join(', ')}
`);

const pairingError = ({ message }) => {
  if (message === 'ERROR: This string is from your badge.') {
    return '⚠ That one is mine. Try again with a code generated from your badge.';
  }

  return `⚠ ${message}`;
};

process.stdout.write('🔌 Connecting to badge...');
badge.connect(() => {
  process.stdout.write('🔌 Badge disconnected!\n');
  process.exit(1);
}).then(async (badge) => {
  process.stdout.write(`OK\n`);
  process.stdout.write(verboseBadgeStatus({ status: await badge.status() }));
  store.subscribe(async () => {
    const state = store.getState();

    if (!state.isInitialized) {
      return;
    }

    const { indexes: { resources: indexes } } = state;
    const self = indexes.by.id[indexes.by.type.self[0].relationships.identity.id][0];

    if (!self.attributes.displayName.startsWith('DC29 Badge')) {
      store.dispatch(setName({ name: `DC29 Badge ${self.id.substring(0, 7)}` }));
      store.dispatch(joinNetwork({ url: 'wss://cedar.hideout.chat:8975' }));
      process.stdout.write('📡 Connecting to badge network...');
    }

    if (networkState.isConnected && !state.connection) {
      process.stdout.write('📡 Lost connection to badge network! Reconnecting...');
      networkState.isConnected = false;
      store.dispatch(joinNetwork({ url: 'wss://cedar.hideout.chat:8975' }));
    }

    if (state.connection && !networkState.isConnected) {
      process.stdout.write('OK\n✅ Ready for peer discovery!\n');
      networkState.isConnected = true;
    }

    await Promise.all((indexes.by.type.topic || []).map(async (topic) => {
      if (!badgePairings[topic.id]) {
        badgePairings[topic.id] = { seenCodes: {} };
      }

      const badgePairing = badgePairings[topic.id];

      if (badgePairing.isBusy) {
        return;
      }

      if (!badgePairing.isRequestSent) {
        process.stdout.write('✨ Another badge has been found on the network!\n');
        badgePairing.isBusy = true;
        // TODO: Check if this topic is ready to attempt to send a message instead of hard-coding a 1s wait.
        await new Promise((resolve) => setTimeout(resolve, 1000));
        store.dispatch(sendMessage({ text: status({ status: await badge.status() }), topic }));
        store.dispatch(sendMessage({ text: greeting({ code: await badge.generate() }), topic }));
        process.stdout.write('💕 Sent pairing request.\n');
        badgePairing.isRequestSent = true;
        badgePairing.isBusy = false;
      } else {
        const codes = (indexes.by.type.message || []).reduce((received, message) => {
          if (message.relationships.topic.id === topic.id) {
            if (message.relationships.sender.id !== self.id) {
              return received.concat(message.attributes.text.split('\n').filter((line) => (/^.*([0-9A-F]{32}).*$/g).test(line)).reduce((codes, line) => {
                const code = line.replace(/^.*([0-9A-F]{32}).*$/g, '$1');

                if (code && code.length === 32 && !badgePairing.seenCodes[code]) {
                  codes.push(code);
                }

                return codes;
              }, []));
            }
          }
          return received;
        }, []);

        for (const code of codes) {
          badgePairing.seenCodes[code] = true;
          badgePairing.isBusy = true;

          try {
            const replyCode = await badge.receive(code);

            if (replyCode) {
              process.stdout.write('💘 Received pairing request from another badge.\n');
              store.dispatch(sendMessage({ text: yourReplyCode({ code: replyCode }), topic }));
            } else {
              process.stdout.write(`🎉 Badge paired! ${status({ status: await badge.status() })}\n`);
              store.dispatch(sendMessage({ text: confirmReplyReceived(), topic }));
            }
          } catch (error) {
            process.stdout.write(`🔥 Pairing error: ${error.message}\n`);
            store.dispatch(sendMessage({ text: pairingError({ message: error.message }), topic }));
          }

          badgePairing.isBusy = false;
        }
      }
    }));
  });

  store.dispatch(initialize());

  process.on('SIGINT', async () => {
    try {
      await badge.disconnect();
    } catch (error) {
      // Nothing we can do at this point.
    }
    process.exit(0);
  });
}).catch((error) => {
  process.stdout.write(`failed!\n🔥 ${error.message}\n`);
  process.exit(1);
});
