# Fully Automated Pairing (FAP) | Badge Hacking | DEF CON 29

## Prerequisites

 * You have [Node.js][1] 16+ installed.
 * You are on a Linux operating system. (this tool **may** work with Windows, but has not been tested on it)
 * You can connect to your badge -- without `sudo` -- via `screen /dev/ttyACM0` (or whatever your badge's device path is).

## Usage

```bash
$ npx dc29-badge-fap@0.1.4
```

## How It Works

This tool interfaces with the DC29 badge and automates
the interactions you would otherwise navigate manually
via a `screen` session with your badge.

For discovering and pairing with other badges, it joins
the [Hideout][2] network (a federated encrypted P2P
messaging protocol).

Whenever another badge is discovered, this tool will
send a pairing request from your badge.

whenever a pairing request code is received, this tool
will create a reply code for that request and send it
to the other badge.

Whenever a pairing reply code is received, this tool will
send that reply code to your badge and try to complete the
pairing process.

[1]: https://nodejs.org/
[2]: https://hideout.chat/
